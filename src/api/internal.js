// ============================================================
// Routes
function declare(app) {
    app.get('/_/status', getStatus);
}

function getStatus(req, res) {
    res.send('OK');
}

// ============================================================
// Exports
export {
    declare,
};

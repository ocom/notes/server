// ============================================================
// Import modules
import { declare as declareInternal } from './internal';


// ============================================================
// Functions
/**
 * Initialize the application with all the routes
 * @param {Express} app
 */
function declare(app) {
    app.get('/date', getDate);
    app.get('/error', getError);
    declareInternal(app);
}

function getDate(req, res) {
    res.json(new Date());
}

function getError() {
    throw new Error('Fake error');
}

// ============================================================
// Exports
export {
    declare,
};

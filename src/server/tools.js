// ============================================================
// Import packages
import SwaggerParser from 'swagger-parser';
import swStats from 'swagger-stats';
import swaggerUi from 'swagger-ui-express';

// ============================================================
// Import modules
import { getApplicationName } from './helpers';

// ============================================================
// Functions

async function initialize({ app, params }) {
    const api = params.apiPath
        ? await loadSwagger(params.apiPath)
        : undefined;

    app.use(swStats.getMiddleware({
        name: getApplicationName(),
        uriPath: '/_',
        swaggerSpec: api,
    }));

    // Swagger UI
    if (api) {
        const options = {
            displayOperationId: true,
            explorer: false,
        };
        app.use('/_/api', swaggerUi.serve, swaggerUi.setup(api, options));
    }
}

// ============================================================
// Helpers
async function loadSwagger(apiPath) {
    const api = await SwaggerParser.parse(apiPath);

    await SwaggerParser.validate(api);

    return api;
}

// ============================================================
// Exports
export {
    initialize,
};

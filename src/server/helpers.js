// ============================================================
// Import modules
import config from 'config';

// ============================================================
// Functions
function getApplicationName() {
    if (config.has('application-name')) {
        return config.get('application-name');
    }

    return require('../../package').name; // eslint-disable-line global-require
}

// ============================================================
// Exports
export {
    getApplicationName,
};

// ============================================================
// Import packages
import config from 'config';
import express from 'express';
import helmet from 'helmet';

// ============================================================
// Import modules
import * as errorTracking from './errorTraking';
import * as logging from './logging';
import * as tools from './tools';
import { declare as declareRoutes } from '../api';

// ============================================================
// Functions

/**
 * Initialize the server
 * @param {Express} app
 * @param {Object}  params
 * @returns {Promise<Logger>}
 */
async function initialize(app, params) {
    // Security
    app.use(helmet());

    // Logging
    const logger = logging.initialize({ app });
    logConfigurationSources(logger);

    // Error tracker
    errorTracking.initialize({ app, logger });

    // Tools
    await tools.initialize({ app, params });

    // Routes
    declareRoutes(app);

    // Finalize error tracker configuration
    errorTracking.finalize(app);

    return logger;
}

/**
 * Log the source of the configuration object
 * @param {Logger} logger
 */
function logConfigurationSources(logger) {
    const sources = config.util.getConfigSources().map(({ name }) => name);

    logger.info(`Configuration sources:\n\t- ${sources.join('\n\t- ')}`, sources);
}

/**
 * Start the server
 *
 * @param {Object} params
 * @param {string} params.apiPath  - Path to the OpenAPI service specification
 * @param {string} params.hostname - Hostname served by the service
 * @param {number} params.port     - Port served by the service
 * @returns {Promise}
 */
async function start(params) {
    const app = express();

    const logger = await initialize(app, params);

    await new Promise((resolve, reject) => {
        app.listen(params.port, params.hostname, (error) => {
            if (error) {
                reject(error);
            }
            else {
                resolve();
            }
        });
    });

    logger.info(`Server listening ${params.hostname}:${params.port}`);
}

// ============================================================
// Exports
export {
    start,
};

// ============================================================
// Import packages
import config from 'config';
import * as Sentry from '@sentry/node';

// ============================================================
// Functions
/**
 * Initialize Sentry
 * @param {Logger} logger
 * @param {Object} params
 * @param {Express} app
 */
function initialize({ app, logger }) {
    if (!config.has('errorTracker.dsn')) {
        logger.info('Error tracker not configured: no configuration provided');
        return;
    }

    Sentry.init({ dsn: config.get('errorTracker.dsn') });

    app.use(Sentry.Handlers.requestHandler());
    logger.info('Error tracker configured');
}

/**
 * Finalize the error tracker
 * @param {Express} app
 */
function finalize(app) {
    if (!config.has('errorTracking.dsn')) {
        return;
    }

    app.use(Sentry.Handlers.errorHandler());
}

// ============================================================
// Exports
export {
    finalize,
    initialize,
};

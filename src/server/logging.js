// ============================================================
// Import packages
import bunyan from 'bunyan';
import bformat from 'bunyan-format';

// ============================================================
// Import modules
import { getApplicationName } from './helpers';

// ============================================================
// Function
/**
 * Initialize server logging
 * @param {Express} app
 * @returns {Logger}
 */
function initialize({ app }) {
    const formatOut = bformat({ outputMode: 'short' });

    const logger = bunyan.createLogger({
        name: getApplicationName(),
        stream: formatOut,
    });

    process.on('uncaughtException', (err) => {
        logger.error(err);
    });

    app.use(requestLogger.bind(undefined, logger));

    return logger;
}

function requestLogger(logger, req, res, next) {
    const message = `${req.method}: ${req.path}`;
    logger.info(message);

    next();
}

// ============================================================
// Exports
export {
    initialize,
};

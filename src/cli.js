#!/usr/bin/env node
/* eslint-disable import/first,global-require */
// ============================================================
// Import packages
import dotenv from 'dotenv';

import path from 'path';
import yargs from 'yargs';

// ============================================================
// Functions

function cli() {
    return yargs
        .usage('$0 <cmd> [args]')
        .command(
            'start', 'Start the server',
            {
                'api-path': {
                    default: './api.yml',
                    description: 'Path to the OpenApi file',
                    normalize: true,
                    requiresArg: true,
                },
                env: {
                    description: 'Path to a .env file to load',
                    normalize: true,
                    env: true,
                    requiresArg: true,
                },
                hostname: {
                    alias: ['h', 'host'],
                    default: '0.0.0.0',
                    description: 'Hostname served by the ser ver',
                    requiresArg: true,
                },

                port: {
                    alias: 'p',
                    default: process.env.PORT || 3000,
                    description: 'Port on which the server will be started',
                    demandOption: !process.env.PORT || 3000,
                    number: true,
                    requiresArg: true,
                },
            },
            start,
        )
        .help()
        .argv;
}


async function start({
    env,
    hostname,
    port,
    'api-path': apiPath,
}) {
    // Loading the .env file
    dotenv.config({
        path: toAbsolute(env),
    });

    // Starting the server
    await require('./server').start({
        apiPath: toAbsolute(apiPath),
        hostname,
        port,
    });
}

// ============================================================
// Helpers
function toAbsolute(filePath) {
    if (!filePath || path.isAbsolute(filePath)) {
        return filePath;
    }

    return path.resolve(filePath);
}

// ============================================================
// Main
cli();

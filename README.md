# starter.express

[![pipeline status](https://gitlab.com/AbrahamTewa/starter.express/badges/develop/pipeline.svg)](https://gitlab.com/AbrahamTewa/starter.express/commits/develop)
[![coverage report](https://gitlab.com/AbrahamTewa/starter.express/badges/develop/coverage.svg)](https://gitlab.com/AbrahamTewa/starter.express/commits/develop)

Simple node starter with express, using Babel 7, gulp 4, unit testing, eslinting, etc...

## Table of Contents

* [Description](#description)
  * [Development experience](#development-experience)
  * [Code quality](#code-quality)
  * [Unit testing, with coverage](#unit-testing-with-coverage)
  * [Continuous Integration](#continuous-integration)
  * [Documentation](#documentation)
* [Install](#install)
  * [Setup CI](#setup-CI)
* [Usage](#usage)
  * [Available commands](#available-commands)
* [Continuous Integration](#continuous-integration)
* [Unit tests](#unit-tests)
 

## Description

This project is meant to be a nodejs express starter. It expose a simple CLI that start a server exposing a `/date` route. But all main development aspects are included in this project.
It is build on top of [starter.nodejs](https://gitlab.com/AbrahamTewa/starter.nodejs) project

### Server features
- Configuration using [node-config](http://lorenwest.github.io/node-config/)
- Environment variable declaration with [dotenv](https://github.com/motdotla/dotenv)
- [Bunyan](https://github.com/trentm/node-bunyan#readme) for logging
- [Sentry](https://sentry.io) integration
- [Swagger-stats](https://swaggerstats.io/)
- [Swagger ui Express](https://github.com/scottie1984/swagger-ui-express) exposing the API of the server. See `api.yml` file

### Development experience
- [Babel 7](http://babeljs.io/), using the `@babel/env` preset.
- [gulp 4](https://github.com/gulpjs/gulp/blob/v4.0.0/docs/API.md) for task creation.
- [conventional commits](http://conventionalcommits.org/) with [semantic-release](https://semantic-release.gitbook.io/)
- [nodemon](https://nodemon.io/)

### Code quality
- [ESLint 6](http://eslint.org/) with [Airbnb base rules](https://www.npmjs.com/package/eslint-config-airbnb-base). The only change on the airbnb rules is the indent rule, set to 4. Just update `.eslintrc` file to change this behavior.
- [EditorConfig](http://editorconfig.com/), just because it's a must-have.
- [remark-lint](https://github.com/remarkjs/remark-lint) configured with recommended rules

### Unit testing, with coverage
Complete unit-testing environment :
- [Mocha](https://mochajs.org/) as test framework
- [Chai](http://chaijs.com/) as assertion library
- [chai-jest-snapshot](https://www.npmjs.com/package/chai-jest-snapshot) for snapshot usage
- [sinon.js](https://sinonjs.org/) for spies, stubs and mocks
- [proxyquire](https://www.npmjs.com/package/proxyquire) to proxyfy modules and packages
- [faker](https://www.npmjs.com/package/faker) to generate fake data

See `src/helpers.spec.js` for a complete example of all these tools.

### Continuous Integration
- [Travis](http://travis-ci.org/) integration, with jobs for:
  - Unit tests (all branches except `master`)
  - Coverage (`master` branch only) reported to [coveralls.io](https://coveralls.io/)
  - Lint
  - Release
  - Github Pages
- [Codacy](https://codacy.com) integration

### Documentation
- [jsDoc](http://usejsdoc.org/) with [minami theme](https://www.npmjs.com/package/minami) for documentation generation.
- [Docsify](http://docsify.js.org/), publish on Github pages

## Install

```bash
git clone https://gitlab.com/AbrahamTewa/starter.express.git
cd starter.express
npm ci
npm run build
```

### Setup CI

In Gitlab CI, you need to setup the following variables for your project:

* `GITHUB_TOKEN`
* `NPM_TOKEN`

#### Semantic Release
Semantic-release is used to update the repository with changelog, updated version of `package.json` and tag creation each time an update is done on master.

To configure the tool, you must defined the following environment variables:

* `GITLAB_TOKEN` with your [private gitlab token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).
* `NPM_TOKEN`, with an npm access token.

See:
* [Semantic-release/github configuration](https://github.com/semantic-release/github#configuration)

## Usage

This command will display the CLI help:
```bash
npm start -- --help
```

### Available commands

#### `npm start`
Start the server.


#### `npm run build`
Run the build of the application.
The builder will create a new folder "build" in which the build will be added.

```bash
npm run build
```

#### `npm run lint`
Lint source files using [ESLint](http://eslint.org)/.

#### `npm run test`
Run the test using [Mocha](https://mochajs.org/).

Output directory : `reports/tests/unit/`.

The output directory it's clean at the begining of the command.

#### `npm run test-cov`
Run tests with coverage using [Istanbul/nyc](https://istanbul.js.org/).

```bash
npm run test-cov
```

This command will generate:
* Unit tests reports as describe bellow
* `./reports/tests/unit/coverage/lcov.info' : lcov file
* `./reports/tests/unit/coverage/lcov-report/index.html' : HTML report file

The output directory it's clean at the begining of the command.

## Continuous Integration
Travis is already configured with several jobs and stages:

| Stage       | Job           | Description                                                         |
| --- | --- | --- |
| Validation  | Unit tests    | Run unit testing                                                    |
| Validation  | Lint          | Run code linting                                                    |
| Publication | Documentation | (`master` branch only) Create and publish the project documentation |

## Unit tests

Unit tests will generate several files:

| Path                                       | Description                                                        |
| --- | --- |
| `dist/`                                    | Folder containing build files. See   [npm build](#npm-run-build) command |
| `reports/tests/unit/xunit.xml`             | JUnit report of unit tests                                         |
| `reports/tests/unit/results/`              | HTML report of unit tests                                          |
| `reports/tests/unit/coverage/lcov.info`    | LCov coverage report of unit tests                                 |
| `reports/tests/unit/coverage/lcov-report/` | HTML coverage report of unit tests                                 |
